/*
 * Copyright (C) 2021 by it's contributors.
 * Some rights reserved. See COPYING, CREDITS.
 *
 * This file is part of RCF Energyloss GUI.
 *
 * RCF Energyloss GUI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * long with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "mex.h"
#include "matrix.h"
#include <math.h> 

#define min_own(a,b) \
   ( a < b ? a : b )

#define max_own(a,b) \
   ( a > b ? a : b )
   
#ifndef __GETTIMEOFDAY_C
#define __GETTIMEOFDAY_C
 
#if defined(_MSC_VER) || defined(_WINDOWS_)
   #include <time.h>
   #include <windows.h>
   
   #if !defined(_WINSOCK2API_) && !defined(_WINSOCKAPI_)
         struct timeval 
         {
            long tv_sec;
            long tv_usec;
         };
   #endif 
         
#else
   #include <sys/time.h>
#endif
         
#if defined(_MSC_VER) || defined(_WINDOWS_)
   int gettimeofday(struct timeval* tv, int empty) 
   {
      union {
         long long ns100;
         FILETIME ft;
      } now;
     
      GetSystemTimeAsFileTime (&now.ft);
      tv->tv_usec = (long) ((now.ns100 / 10LL) % 1000000LL);
      tv->tv_sec = (long) ((now.ns100 - 116444736000000000LL) / 10000000LL);
     return (0);
   }   
#endif
   
#endif /* __GETTIMEOFDAY_C */


   
/*evaluate spline-function for given xik-value*/
double evalspline(double *x, double *c, int kval, int M, int nx, double xik)
{
    #define CM(i,j) c[i+j*M]
    int i1=0,i9=0,imid=0,index=0,i=0;
    double y1=0;
              
    i1=0;
    i9=nx-1;
       
    if (xik<=x[0]) index=0;
    else if (xik>=x[nx-1]) index=nx-2;
       
    else {
        while (i9>i1+1) {
            imid = (i1+i9+1)/2;
            if (x[imid]<xik) i1=imid;
            else i9=imid;
        }
           
        index=i1;
    }
       
    xik = xik - x[index];
    y1=CM(0,index);           
            
    for(i=1; i<=kval-1; i++) {
        y1 = xik*y1 + CM(i, index);
    }
       
    return(y1);
 }
   
/*generates uniform distributed [0,1) random numbers*/
double unirnd()
{
    double out=0;
    
    out=(double)rand()/(double)RAND_MAX;
    return(out);
}
   
/*generates poisson distributed random numbers by inverse transformation method*/
int poisrnd(double lambda)
{  
    int k=0;
    const double target=exp(-lambda);
    double p=(double)rand()/(double)RAND_MAX;
  
    while (p>target)
    {
        p*=(double)rand()/(double)RAND_MAX;
        k+=1;
    }

    return(k);
}
   
/*generates gaussian distributed random numbers Knuth Sec. 3.4.1 p. 117*/
double gaussrnd(double mean, double sigma)
{
	static double V1, V2, S;
	static int phase = 0;
	double X;

	if(phase == 0) {
		do {
			double U1 = (double)rand()/(double)RAND_MAX;
			double U2 = (double)rand()/(double)RAND_MAX;

			V1 = 2 * U1 - 1;
			V2 = 2 * U2 - 1;
			S = V1 * V1 + V2 * V2;
			} while(S >= 1 || S == 0);

		X = V1 * sqrt(-2 * log(S) / S);
	} else
		X = V2 * sqrt(-2 * log(S) / S);

	phase = 1 - phase;
            
	return(X*sigma+mean); 
}

/*generates gamma distributed random numbers => 
 * gammarnd(a,b) generator - code snippet extracted from GSL library
 * with a=shape parameter and b=scale parameter*/
double gammarnd(double a, double b)
{
    /*assume a > 0*/
    if (a < 1)
    {
        double u = (double)rand()/(double)RAND_MAX;
        return(gammarnd(1.0+a, b)*pow(u, 1.0/a));
    }
    
    {
        double x, v, u;
        double d = a-1.0/3.0;
        double c = (1.0/3.0)/sqrt(d);
        
        while (1)
        {
            do
            {
            x = gaussrnd(0.0, 1.0);
            v = 1.0+c*x;
            }
            
            while (v <= 0);
            v = v*v*v;
            u = (double)rand()/(double)RAND_MAX;
            
            if (u < 1-0.0331*x*x*x*x) 
                break;
            
            if (log (u) < 0.5*x*x+d*(1-v+log(v)))
                break;
        }
        
        return(b*d*v);
    }
}

/*calculate step-size limit for continous energy-loss => based on GEANT4*/
double stepsize(double range, double restlength)
{
    /*units are mm*/
    /*finaleRange = 500nm; dRoverRange = 1% 
     => benchmarked against TRIM, compromise between
     accuracy and speed; smaller step-size than
     GEANT default 50um and 20% => approx 30times
     more steps*/
    double finalRange=5.0e-4;
    double dRoverRange=0.01;
    double stepsize=0.;    
    
    if (range>finalRange)
    {
        stepsize=dRoverRange*range+finalRange*(1.0-dRoverRange)*(2.0-finalRange/range);
    }
    else
    {
        stepsize=range;
    }
    
    /*to fit exactly on boundary of layer*/
    if (restlength<stepsize)
    {
        stepsize=restlength;
    }
    
    return(stepsize);    
}

/*calculates energyloss fluctuation based on GEANT4 model*/
double fluctuate(double length, double meanLoss, double eleden, double effcha, double excen, double energy, double projmass)
{
    /*The basic units are :
     * millimeter  (mm)
     * Mega electron Volt  (MeV)
     * positon charge (eplus)*/
    
    double e1=0., e2=0., particleMass=0.;
    int istep, k;
       
    /*constants*/
    double pi=3.14159265358979323846;  
    double minLoss=10.0e-6;
    double electron_mass_c2=0.51099906;
    double classic_electr_radius = 2.8179403267e-12;    
    double twopi_mc2_rcl2=2.0*pi*electron_mass_c2*classic_electr_radius*classic_electr_radius;
    double ratio=0., electronDensity=0., Zeff=0., ipotFluct=0., chargeSquare=0., tkin=0.;
    double tau=0., gam=0., gam2=0., beta2=0.;
  
    double rate=0.55;
    double nmaxCont=16.;
    double fw=4.;
    
    double minNumberInteractionsBohr, loss, siga;
    double tmax=0., massrate=0., tmaxkine=0., sn=0., twomeanLoss=0., neff=0.;
    
    double f2Fluct=0., f1Fluct=0., ipotLogFluct=0., e2Fluct=0., e2LogFluct=0.;
    double e1LogFluct=0., e1Fluct=0., esmall=0., e0=0., losstot=0.;
    int nstep, nb;
    double a1=0., a2=0., a3=0.;
    double w=0., w2=0., w1=0., C=0., sa1=0.;
    double emean = 0.;
    double sig2e = 0., sige = 0.;
    double p1 = 0., p2 = 0., p3 = 0.;
    double lossc=0.;
    double alfa = 1., alfa1=0., namean=0.;
    
    /*projectile specific values*/
    if (projmass==1.0)
    {
        /*proton*/
        particleMass=938.27231;       
    }
    else
    {
        /*deuteron*/
        particleMass=1875.612793;
    }
    
    /*printf ("mass: %f \n", particleMass);*/
    
    ratio=electron_mass_c2/particleMass;
    chargeSquare=1.0;
    tkin  = energy*1.0e-3;
    
    /*target specific values, test: Alu-case
    double electronDensity=7.834140347e20;
    double Zeff=13.0;
    double ipotFluct=170.0e-6;*/
    
    /*target specific values, test: Cu-case
    double electronDensity=2.451464308e21;
    double Zeff=29.0;
    double ipotFluct=332.0e-6;*/
        
    /*target specific values, test: Polyester-case
    double electronDensity=4.230609526e20;
    double Zeff=4.5453;
    double ipotFluct=60.46122e-6;*/
    
    /*target specific values*/
    electronDensity=eleden;
    Zeff=effcha;
    ipotFluct=excen*1.0e-6;
    
        
    if (meanLoss < minLoss) { return(meanLoss); }
    
    tau   = tkin/particleMass;
    gam   = tau + 1.0;
    gam2  = gam*gam;
    beta2 = tau*(tau + 2.0)/gam2;
    
    minNumberInteractionsBohr=10.0;
    
    loss=0.0;
    siga=0.0;
    tmax=0.;
    massrate=0.;
    tmaxkine=0.;
    sn=0.;
    twomeanLoss=0.;
    neff=0.;
        
    /*maximum energy transferable in "one" collision in MeV 
     * => we have no straggling sampled due to discrete delta ray production
     * => continuous energy loss has energy fluctuations => energy straggling*/
    tmax = 2.0*electron_mass_c2*tau*(tau + 2.)/(1. + 2.0*(tau + 1.)*ratio + ratio*ratio);
       
     /*Gaussian regime
     for heavy particles only and conditions
     for Gauusian fluct. has been changed */
    
    if ((particleMass > electron_mass_c2) &&
            (meanLoss >= minNumberInteractionsBohr*tmax))
    {
        massrate = electron_mass_c2/particleMass ;
        tmaxkine = 2.*electron_mass_c2*beta2*gam2/(1.+massrate*(2.*gam+massrate)) ;
        
        if (tmaxkine <= 2.*tmax)   
        {
            siga = sqrt((1.0/beta2 - 0.5) * twopi_mc2_rcl2 * tmax * length * electronDensity * chargeSquare);

            sn = meanLoss/siga;

            /*thick target case*/
            if (sn >= 2.0) {
                twomeanLoss = meanLoss + meanLoss;
                
                do {
                    loss = gaussrnd(meanLoss,siga);
                } while (0.0 > loss || twomeanLoss < loss);
                
                /*Gamma distribution => (shape, inverse scale)*/
            } else {
                neff = sn*sn;
                loss = meanLoss*gammarnd(neff,1.0)/neff;
            }
            
            return(loss);
        }
    }
    
    /*Glandz regime : initialisation*/    
    if (Zeff > 2.) f2Fluct = 2./Zeff ;
    else         f2Fluct = 0.;
    
    f1Fluct = 1. - f2Fluct;
    ipotLogFluct = log(ipotFluct);
    
    e2Fluct = 10.0e-6*Zeff*Zeff;
    e2LogFluct = log(e2Fluct);

    e1LogFluct = (ipotLogFluct - f2Fluct*e2LogFluct)/f1Fluct;
    e1Fluct = exp(e1LogFluct);
   
    e0 = 10.0e-6;
    esmall = 0.5*sqrt(e0*ipotFluct);  
    
    /*very small step or low-density material*/
    if(tmax <= e0) { return(meanLoss); }
    
    losstot = 0.;
    nstep = 1;
    
    if(meanLoss < 25.*ipotFluct)
    {
        if(unirnd() < 0.04*meanLoss/ipotFluct)
        { nstep = 1; }
        else
        { 
            nstep = 2;
            meanLoss *= 0.5; 
        }
    }
 
    for(istep=0; istep < nstep; ++istep) {

        loss = 0.;
        
        a1 = 0.; a2 = 0.; a3 = 0. ;
        w=0., w2=0., w1=0., C=0., sa1=0.;
          
        if(tmax > ipotFluct) {
            w2 = log(2.*electron_mass_c2*beta2*gam2)-beta2;
            
            if(w2 > ipotLogFluct)  {
                C = meanLoss*(1.-rate)/(w2-ipotLogFluct);
                a1 = C*f1Fluct*(w2-e1LogFluct)/e1Fluct;
                if(w2 > e2LogFluct) {
                    a2 = C*f2Fluct*(w2-e2LogFluct)/e2Fluct;
                }
                
                if(a1 < nmaxCont) { 
                    /*small energy loss*/
                    sa1 = sqrt(a1);
                    if(unirnd() < exp(-sa1))
                    {
                        e1 = esmall;
                        a1 = meanLoss*(1.-rate)/e1;
                        a2 = 0.;
                        e2 = e2Fluct;
                    }
                    else
                    {
                        a1 = sa1;    
                        e1 = sa1*e1Fluct;
                        e2 = e2Fluct;
                    }
                    
                } else {
                    /*not small energy loss
                     * correction to get better fwhm value*/
                    a1 /= fw;
                    e1 = fw*e1Fluct;
                    e2 = e2Fluct;
                }
            }   
        }
        
        w1 = tmax/e0;
        if(tmax > e0) {
            a3 = rate*meanLoss*(tmax-e0)/(e0*tmax*log(w1));
        }
        /*'nearly' Gaussian fluctuation if a1>nmaxCont&&a2>nmaxCont&&a3>nmaxCont*/
        emean = 0.;
        sig2e = 0.; sige = 0.;
        p1 = 0.; p2 = 0.; p3 = 0.;
        
        /*excitation of type 1*/
        if(a1 > nmaxCont)
        {
            emean += a1*e1;
            sig2e += a1*e1*e1;
        }
        else if(a1 > 0.)
        {
            p1 = (double)poisrnd(a1);
            loss += p1*e1;
            if(p1 > 0.) {
                loss += (1.-2.*unirnd())*e1;
            }
        }
        
        /*excitation of type 2*/
        if(a2 > nmaxCont)
        {
            emean += a2*e2;
            sig2e += a2*e2*e2;
        }
        else if(a2 > 0.)
        {
            p2 = (double)poisrnd(a2);
            loss += p2*e2;
            if(p2 > 0.) 
                loss += (1.-2.*unirnd())*e2;
        }
        
        if(emean > 0.)
        {
            sige   = sqrt(sig2e);
            loss += max_own(0.,gaussrnd(emean,sige));
        }
        
        /*ionisation*/
        lossc = 0.;
        if(a3 > 0.) {
            emean = 0.;
            sig2e = 0.;
            sige = 0.;
            p3 = a3;
            alfa = 1.; alfa1=0.; namean=0.;
            if(a3 > nmaxCont)
            {
                alfa            = w1*(nmaxCont+a3)/(w1*nmaxCont+a3);
                alfa1  = alfa*log(alfa)/(alfa-1.);
                namean = a3*w1*(alfa-1.)/((w1-1.)*alfa);
                emean          += namean*e0*alfa1;
                sig2e          += e0*e0*namean*(alfa-alfa1*alfa1);
                p3              = a3-namean;
            }

            w2 = alfa*e0;
            w  = (tmax-w2)/tmax;
            nb = poisrnd(p3);
            if(nb > 0) {
                for (k=0; k<nb; k++) lossc += w2/(1.-w*unirnd());
            }
        }
        
        if(emean > 0.)
        {
            sige   = sqrt(sig2e);
            lossc += max_own(0.,gaussrnd(emean,sige));
        }
        
        loss += lossc;
        
        losstot += loss;
    }

    return(losstot);
    
}


void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
    /*called from matlab like: 
    a=elo(energy, thickness, xvec, cvec, crangevec, xrangeinvvec, crangeinvvec, k, Nele, Zeff, Ieff, projmass, iscorr, ccorrvec);
    or
    a=elo(energy, thickness, xvec, cvec, crangevec, xrangeinvvec, crangeinvvec, k, Nele, Zeff, Ieff, projmass, 0);
     
    with
    a(:,1) -> gives new output energy vector for each MonteCarlo particle "after" material
    a(:,2) -> gives energyloss vector for each MonteCarlo particle in material
    a(:,3) -> gives energloss*active layer correction (RCF efficency) vector for each MonteCarlo particle; if isscor=1; if iscore=0 -> gives 0
     
    energy = input energy vector for each MonteCarlo particle !
    thickness = material thickness, one double value !
     
    xvec = breaks-vector from common Eloss-Table spline-fit, transposed before;
            like fit=spline(x,y) => xvec=(fit.breaks)';
    cvec = coefs-matrix from common Eloss-Table spline-fit of matlab, but transposed before; 
            like fit=spline(x,y) => cvec=(fit.coefs)';

    crangevec = coefs-matrix from common Range-Table spline-fit of matlab, but transposed before;
    (break vector same like xvec, see below...)
    
    xrangeinvvec = breaks-vector from common Inverse-Range-Table spline-fit, transposed before;
    crangeinvvec = coefs-matrix from common Inverse-Range-Table spline-fit of matlab, but transposed before; 
          
    k = one double value; order of spline fit; fit.order; for cubic-spline k=4
    
    Nele = one double value; effective electron density per mm^3 of material
    
    Zeff = one double value; effective nuclear charge of material
    
    Ieff = one double value; effective ionization potential of material
    
    projmass = one double value; Mass of projectile; at the moment: projmass=1.0=proton, projmass=2.0=deuteron 
     
    iscorr = 0 or 1 (false or true) => should RCF-efficency be calculated?
    if iscorr = 0, cveccorr not needed!
     
    if iscorr = 1:
    cveccorr are coefs-matrix for correction; transposed before!
    => needs to have same break-points and dimension, will be take care in srim-read!
    (cvecorr have same "geometry" for fit like cvec; see srim-reed!) 
    */
          
    const mxArray *E_newgrid, *thicknessgrid, *cgrid, *crangegrid, *xrangeinvgrid;
    const mxArray *crangeinvgrid, *ccorrgrid, *xgrid, *kgrid, *iscorrgrid;
    const mxArray *eledengrid, *effchagrid, *excengrid, *projmassgrid; 
    mxArray *idx;
    
    double *idxptr, *E_new, *thickness, *closs, *crange, *ccorr, *x, *xrangeinv;
    double *crangeinv, *k, *iscorr, *eleden, *effcha, *excen, *projmass;
    double E_new_out, E_in, thicknessval, position, range;
    double eledenval, effchaval, excenval, projmassval, invrange;
    double eloss=0., dEdx=0., dEdxcorr=0., Estop=0., Estopcorr=0., stepsz=0., qcorr=0.;
    int kval=0, nx=0, M=0, nMC=0, iscorrval=0, j=0, stepsdone=0;
    mwSize dims[2];
    
    struct timeval time;
    
    /*linear loss limit*/
    double eta=0.01;
    
    /*catch pointer input energy and size of vector*/
    E_newgrid = prhs[0];
    E_new = mxGetPr(E_newgrid);
    nMC=mxGetM(E_newgrid);    
   
    /*catch pointer thickness*/
    thicknessgrid = prhs[1];
    thickness = mxGetPr(thicknessgrid);
    thicknessval = thickness[0];
    
    /*catch pointer breaks-vector x for Eloss/Corr/Range-spline
     => same x-vector for all due to Matlab fit!*/
    xgrid = prhs[2];
    x = mxGetPr(xgrid);
    nx = mxGetM(xgrid); 
    
    /*catch pointer coefs-matrix c for Eloss-spline*/
    cgrid = prhs[3];
    closs = mxGetPr(cgrid);
    M = mxGetM(cgrid);
    
    /*catch pointer coefs-matrix c for Range-spline*/
    crangegrid = prhs[4];
    crange = mxGetPr(crangegrid);
    
    /*catch pointer breaks-vector x for RangeInverse-spline*/
    xrangeinvgrid = prhs[5];
    xrangeinv = mxGetPr(xrangeinvgrid);
    
    /*catch pointer coefs-matrix c for RangeInverse-spline*/
    crangeinvgrid = prhs[6];
    crangeinv = mxGetPr(crangeinvgrid);
           
    /*catch pointer order of fit*/
    kgrid = prhs[7];
    k = mxGetPr(kgrid);
    kval = k[0];
    
    /*catch pointer for electron density*/
    eledengrid = prhs[8];
    eleden = mxGetPr(eledengrid);
    eledenval = eleden[0];
    
    /*catch pointer for effective charge*/
    effchagrid = prhs[9];
    effcha = mxGetPr(effchagrid);
    effchaval = effcha[0];
    
    /*catch pointer for mean ionization potential*/
    excengrid = prhs[10];
    excen = mxGetPr(excengrid);
    excenval = excen[0];
    
    /*catch pointer for projectile mass 1.0=proton, 2.0=deuteron*/
    projmassgrid = prhs[11];
    projmass = mxGetPr(projmassgrid);
    projmassval = projmass[0];
    
    /*catch pointer: calc correction; true=1 false=0*/
    iscorrgrid = prhs[12];
    iscorr = mxGetPr(iscorrgrid);
    iscorrval = iscorr[0];
    
    /*initialize random seed with time in microseconds */
    gettimeofday(&time, NULL);
    srand(time.tv_usec * time.tv_sec);
    
    /*create output matrix a; size(a)= nMC x 3*/        
    dims[0] = nMC; dims[1] = 3;
    plhs[0] = idx = mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
    if (idx==NULL) 
    {
        dims[0] = 0; dims[1] = 0;
        plhs[0] = mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
        return;
    }
    
    /*assign output to righthand pointer*/
    idxptr = mxGetPr(idx);
    
   
    /*calculate output values for no correction*/
    if (iscorrval==0)
    {
        /*run over monte-carlo vector*/    
        for (j=0; j<nMC; j++)
        {
            E_new_out=E_new[j];
            E_in=E_new_out;
            Estop=0.0;
            Estopcorr=0.0;
            
            /*units are mm and MeV => keV/µm=MeV/mm*/
            position=thicknessval*1.0e-3;
            
            /*calculate energy-loss and fluctuations*/
            while (position>0.0)
            {
                /*evaluate range (convert to mm) and eloss-spline (keV/um=MeV/mm)*/
                range=evalspline(x, crange, kval, M, nx, E_new_out)*1.0e-3;
                /*printf ("range: %f \n", range*1.0e3);*/
                dEdx=evalspline(x, closs, kval, M, nx, E_new_out); 
                          
                /*calculate new step-size (mm) and energyloss (MeV/mm*mm=MeV)*/
                stepsz=stepsize(range, position);
                /*printf ("stepsize: %f \n", stepsz*1.0e3);*/
                eloss=dEdx*stepsz;                
               
                /*evaluate inverse range-spline => needs um, gives keV*/
                invrange=evalspline(xrangeinv, crangeinv, kval, M, nx, (range-stepsz)*1.0e3);
        
                /*linear loss limit*/
                if (eloss>=eta*E_new_out*1.0e-3)
                {
                    eloss=(E_new_out-invrange)*1.0e-3;
                }                                   
                
                /*decide if routine runs as MonteCarlo with fluctuations
                 *or just single tracking with meanEloss*/
                if (nMC==1)
                {
                    eloss = eloss*1.0e3;
                }
                /*fluctuate energy-loss and convert to keV*/
                else {
                    eloss = fluctuate(stepsz, eloss, eledenval, effchaval, excenval, E_new_out, projmassval)*1.0e3;
                }  
                              
                /*calculate new energy and energy-loss*/
                if (E_new_out >= eloss && E_new_out > 0.0) {
                    E_new_out = E_new_out-eloss;
                    Estop = Estop+eloss;
                    position=position-stepsz;
                }
                /*enegy conservation, particle stops => all energy deposited*/
                else if (E_new_out > 0.0) {
                    eloss = E_in-Estop;
                    E_new_out = 0.0;
                    Estop = Estop+eloss;
                    position=-1.0;
                }
                /*case if input energy is negative*/
                else {
                    eloss = 0.0;
                    E_new_out = 0.0;
                    Estop = Estop+eloss;
                    position=-1.0;
                }            
            }
    
            /*write stuff back to output pointer*/
            idxptr[j]=E_new_out;
            idxptr[j+nMC]=Estop;
            idxptr[j+2*nMC]=Estopcorr;        
        }
        /*printf ("stepsdone: %i \n", stepsdone);*/
        return;
    }
    
    /*calculate output values in case of quenching-correction*/
    else if (iscorrval==1)        
  {
        ccorrgrid = prhs[13];
        ccorr = mxGetPr(ccorrgrid);
        
        /*run over monte-carlo vector*/    
        for (j=0; j<nMC; j++)
        {
            E_new_out=E_new[j];
            E_in=E_new_out;
            Estop=0.0;
            Estopcorr=0.0;
            
            /*units are mm and MeV => keV/µm=MeV/mm*/
            position=thicknessval*1.0e-3;
            
            /*calculate energy-loss and fluctuations*/
            while (position>0.0)
            {
                /*evaluate range (convert to mm) and eloss-spline (keV/um=MeV/mm)*/
                range=evalspline(x, crange, kval, M, nx, E_new_out)*1.0e-3;
                dEdx=evalspline(x, closs, kval, M, nx, E_new_out);
                /*evaluate RCF quenching-correction spline*/
                qcorr=evalspline(x, ccorr, kval, M, nx, E_new_out);
           
                /*calculate new step-size (mm) and energyloss (MeV/mm*mm=MeV)*/
                stepsz=stepsize(range, position);                               
                eloss=dEdx*stepsz;                      
     
                /*evaluate inverse range-spline => needs um, gives keV*/
                invrange=evalspline(xrangeinv, crangeinv, kval, M, nx, (range-stepsz)*1.0e3);
        
                /*linear loss limit*/
                if (eloss>=eta*E_new_out*1.0e-3)
                {
                    eloss=(E_new_out-invrange)*1.0e-3;
           
                }
                
                /*decide if routine runs as MonteCarlo with fluctuations
                 *or just single tracking with meanEloss*/
                if (nMC==1)
                {
                    eloss = eloss*1.0e3;
                }
                /*fluctuate energy-loss and convert to keV*/
                else {
                    eloss = fluctuate(stepsz, eloss, eledenval, effchaval, excenval, E_new_out, projmassval)*1.0e3;
                }               
               
                /*calculate new energy and energy-loss*/
                if (E_new_out >= eloss && E_new_out > 0.0) {
                    E_new_out = E_new_out-eloss;
                    Estop = Estop+eloss;
                    Estopcorr = Estopcorr+(qcorr*eloss);
                    position=position-stepsz;
                }
                /*enegy conservation, particle stops => all energy deposited*/
                else if (E_new_out > 0.0) {
                    eloss = E_in-Estop;
                    E_new_out = 0.0;
                    Estop = Estop+eloss;
                    Estopcorr = Estopcorr+(qcorr*eloss);
                    position=-1.0;
                }
                /*case if input energy is negative*/
                else {
                    eloss = 0.0;
                    E_new_out = 0.0;
                    Estop = Estop+eloss;
                    Estopcorr = Estopcorr+(qcorr*eloss);
                    position=-1.0;
                }            
            }
    
            /*write stuff back to output pointer*/
            idxptr[j]=E_new_out;
            idxptr[j+nMC]=Estop;
            idxptr[j+2*nMC]=Estopcorr;        
        }
        return;
    }
    
}
