function varargout = RCF_energyloss_GUI(varargin)
% RCF_energyloss_GUI
% Purpose: Calculate the energy deposition of laser-accelerated protons in Radiochromic Film detector stacks.
%          The code uses tables for the energy loss produced by the SRIM code package.
%          See http://www.srim.org for details about SRIM.

% Copyright (C) 2013,2021 by it's contributors.
% Some rights reserved. See COPYING, CREDITS.
%
% This file is part of RCF Energyloss GUI.
%
% RCF Energyloss GUI is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% long with this program.  If not, see <https://www.gnu.org/licenses/>.

% Update: 27. April 2016, CB, RCF config as plain text output ...
% Update: 02. September 2019, CB, progress Monitor off, Ver 2.2

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @RCF_energyloss_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @RCF_energyloss_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State,varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before RCF_energyloss_GUI is made visible.
function RCF_energyloss_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to RCF_energyloss_GUI (see VARARGIN)

% Choose default command line output for RCF_energyloss_GUI
handles.output = hObject;

set(handles.print_pushbutton,'Enable','Off');
set(handles.corroff,'Enable','Off');
set(handles.corron,'Enable','Off');

closeit=0;

%define a version number here:
%give program version to handles => can be found in file later
handles.progversion='2.2';

%set per default to protons
handles.parameters.ionspecies='protons';
set(handles.proton_button,'Value',1);
set(handles.deut_button,'Value',0);
update_species(handles);

%hard-coded number of montecarlo particles
% default off
handles.MC_parts=1;
handles.parameters.MC_parts=handles.MC_parts;

% parallel switched off! CB 02.11.2017
%v = ver;
%if any(strcmp('Parallel Computing Toolbox', {v.Name}))
%    handles.parallel = true;
%else
    handles.parallel = false;
%end

%give warning and introduction abount missing mex-file
if exist('elo','file')==0
    cprintf([1 0 0],'ERROR: elo mex-file not found, you have to compile first before running program ! \n \n type: mex elo.c \n \n if you running mex the first time \n type: mex -setup and choose a c-compiler \n after this run: mex elo.c \n\n');
    cprintf([0,0,0],'')
    closeit=1;
else
    new = dir('elo.c');
    old = dir('elo.m*');
    
    %if elo.c is newer mex-file => there was an update, please recompile
    %elo.c
    if new.datenum>=old.datenum
      cprintf([1 0 0],'ERROR: elo mex-file is older than elo.c => there was an update, please recompile elo.c \n \n type: mex elo.c \n \n');
      cprintf([0,0,0],'')
      closeit=1;
      
    else
        if handles.parallel
        % parallel toolbox ...
        % start parallel workers; enable processbar java
            isOpen = parpool('size') > 0;
            if isOpen == 1
              wb=waitbar(0,'Initializing parallel environment...please be patient...');
              waitbar(0.3);
              parpool close force;
              waitbar(0.6);
              parpool open;
              waitbar(0.9);
              pctRunOnAll javaaddpath java;
              waitbar(1);
              close(wb);
            else
              wb=waitbar(0,'Initializing parallel environment...please be patient...'); 
              waitbar(0.6);
              parpool open;
              waitbar(0.9);
              pctRunOnAll javaaddpath java;
              waitbar(1);
              close(wb);
            end
        else
          handles.parallel = false;
        end
        % Update handles structure
        guidata(hObject, handles);
    end    
end

%check for data folder, if nonexistent create it
if ~exist('data', 'dir')
    mkdir('data')
end

set(handles.shotfilename_edit,'String', 'new');
set(handles.rcfconfig_edit,'String', 'H2;');
handles.parameters.shotfilename = get(handles.shotfilename_edit,'String');
handles.parameters.E_min = str2double(get(handles.emin_edit,'String'));
handles.parameters.E_max = str2double(get(handles.emax_edit,'String'));
handles.parameters.E_steps = str2double(get(handles.esteps_edit,'String'));

if rem(length(get(handles.rcfconfig_edit,'String')),3) == 0
    c = textscan(get(handles.rcfconfig_edit,'String') ,'%s','Delimiter',';');
    handles.parameters.rcf_config = c{1}';
end

% --- Show RCF stack here
show_rcfstack(handles);
    
% Update handles structure
guidata(hObject, handles);

if closeit==1
    close RCF_energyloss_GUI
end

% --- Outputs from this function are returned to the command line.
function varargout = RCF_energyloss_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

function emin_edit_Callback(hObject, eventdata, handles)
% hObject    handle to emin_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.parameters.E_min=str2double(get(hObject,'String'));
guidata(hObject, handles);

function emax_edit_Callback(hObject, eventdata, handles)
% hObject    handle to emax_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
loss = getappdata(0, 'loss');
fields = fieldnames(loss);
no = length(fields);
maxE = 0;
tab = '';
for i = 2:no
    maxEt = max(loss.(fields{i}).x);
    if maxEt >= maxE
        maxE = maxEt;
        tab = fields{i};
    end
end
if str2double(get(hObject,'String')) > maxE/1000
    set(hObject, 'String', num2str(maxE/1000));
    show_message(handles,['ERROR: ' tab ' Energy loss is only up to ' num2str(maxE/1000) ' MeV valid'],'red')
end

handles.parameters.E_max=str2double(get(hObject,'String'));
guidata(hObject, handles);

function esteps_edit_Callback(hObject, eventdata, handles)
% hObject    handle to esteps_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.parameters.E_steps=str2double(get(hObject,'String'));
guidata(hObject, handles);

function shotfilename_edit_Callback(hObject, eventdata, handles)
% hObject    handle to shotfilename_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.parameters.shotfilename=get(hObject,'String');
guidata(hObject, handles);

function rcfconfig_edit_Callback(hObject, eventdata, handles)
% hObject    handle to rcfconfig_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if rem(length(get(hObject,'String')),3) == 0
    c = textscan(get(hObject,'String') ,'%s','Delimiter',';');
    handles.parameters.rcf_config = c{1}';
    show_rcfstack(handles);
end
guidata(hObject, handles);

% --- Executes on button press in load_pushbutton.
function load_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to load_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
user=0;

[fn, pn] = uigetfile('*.mat','Load Workspace', 'data');
if ~isequal(fn,0)
    A = load(fullfile(pn, fn));
    
    cla('reset');
    
try
    params = A.params;
    
    % try to load everything from the file...
    try
        handles.parameters.E_min = A.E_min;
    catch
        try
            handles.parameters.E_min = A.handles.parameters.E_min;
        catch
            handles.parameters.E_min = 0.5;
        end
    end
    try
        handles.parameters.E_max = A.E_max;
    catch
        try
            handles.parameters.E_max = A.handles.parameters.E_max;
        catch
            handles.parameters.E_max = 30;
        end
    end
    try
        handles.parameters.E_steps = A.E_steps;
    catch
        try
            handles.parameters.E_steps = A.handles.parameters.E_steps;
        catch
            handles.parameters.E_steps = 0.1;
        end
    end
    try
        handles.parameters.shotfilename = A.shotfilename;
    catch
        try
            handles.parameters.shotfilename = A.handles.parameters.shotfilename;
        catch
            handles.parameters.shotfilename = 'new';
        end
    end
    try
        handles.parameters.rcf_config = A.rcf_config;
    catch
        try
            handles.parameters.rcf_config = A.handles.parameters.rcf_config;
        catch
            handles.parameters.rcf_config = {'H2'};
        end
    end
    try
        handles.parameters.stoppingE = A.stoppingE;
    catch
        try
            handles.parameters.stoppingE = A.handles.parameters.stoppingE;
        catch
            handles.parameters.stoppingE = 0;
        end
    end
    try
        handles.parameters.Edeposition = A.Edeposition;
    catch
        try
            handles.parameters.Edeposition = A.handles.parameters.Edeposition;
        catch
            handles.parameters.Edeposition = 0;
        end
    end
    try
        handles.parameters.EdepositionCu = A.EdepositionCu;
    catch
        try
            handles.parameters.EdepositionCu = A.handles.parameters.EdepositionCu;
        catch
            handles.parameters.EdepositionCu = 0;
        end
    end
    try
        handles.parameters.ionspecies = A.ionspecies;
    catch
        handles.parameters.ionspecies = 'protons';
    end
    try
        handles.parameters.fversion = A.fversion;
    catch
        handles.parameters.fversion = '0';        
    end
    if strcmp(handles.parameters.ionspecies,'deuterons')
        set(handles.proton_button,'Value',0);
        set(handles.deut_button,'Value',1);
    else
        set(handles.proton_button,'Value',1);
        set(handles.deut_button,'Value',0);
    end
    
    update_species(handles);
    set(handles.proton_button,'Enable','Off');
    set(handles.deut_button,'Enable','Off');
               
    %set all parameters from last run
    set(handles.emin_edit,'String',handles.parameters.E_min);
    set(handles.emax_edit,'String',handles.parameters.E_max);
    set(handles.esteps_edit,'String',handles.parameters.E_steps);

    %pre-select list entries
    A = get(handles.mat_L1_edit, 'String');

    for lno=1:8
        set(handles.(['thick_L' num2str(lno) '_edit']), 'String', params.(['L' num2str(lno) '_thick']));
        B = params.(['L' num2str(lno)]);
        if find(strncmp(A, B, 20))
             set(handles.(['mat_L' num2str(lno) '_edit']), 'Value', find(strncmp(A, B, 20)));
        else
             set(handles.(['mat_L' num2str(lno) '_edit']), 'Value', 1);
        end
    end
    
    set(handles.shotfilename_edit,'String',handles.parameters.shotfilename);
    %make rcf_config readable
    set(handles.rcfconfig_edit,'String', cell2mat(strcat(handles.parameters.rcf_config,';')));
    
    %load new - to be compatible to 'old' files
    handles.parameters.stoppingE=zeros(size(handles.parameters.Edeposition,3),2);
    stoppingE=zeros(size(handles.parameters.Edeposition,3),2);
    for i=1:size(handles.parameters.Edeposition,3)
        X=handles.parameters.Edeposition(:,1,i);
        Y=handles.parameters.Edeposition(:,2,i);
        [val, ind] = max(Y);
        stoppingE(i,1) = X(ind);
        stoppingE(i,2) = val;
    end
    
    %backside CR-39
    handles.parameters.stoppingECR=zeros(size(handles.parameters.Edeposition,3),2);
    stoppingECR=zeros(size(handles.parameters.Edeposition,3),2);
    for i=1:size(handles.parameters.Edeposition,3)
        X=handles.parameters.Edeposition(:,1,i);
        Y=handles.parameters.Edeposition(:,3,i);
        [val, ind] = max(Y);
        stoppingECR(i,1) = X(ind);
        stoppingECR(i,2) = val;
    end
    
    handles.parameters.stoppingECR(:,:) = stoppingECR(:,:);
    handles.parameters.stoppingE(:,:) = stoppingE(:,:);
    
    clear val ind X Y stoppingECR stoppingE;
    
    % parallel out
%     v = ver;
%     if any(strcmp('Parallel Computing Toolbox', {v.Name}))
%         handles.parallel = true;
%     else
        handles.parallel = false;
%     end
    % Update handles structure
    guidata(hObject, handles);
    
    %check if old file was calculated without quenching correction
    try 
        if isnumeric(handles.parameters.Edeposition(1,4,1)) && str2double(handles.parameters.fversion) >= str2double(handles.progversion)
          % --- Show RCF stack here
          show_rcfstack(handles);
          % --- show picture
          show_picture(handles);
          user='loaded';
        else
          % --- Show RCF stack here
          show_rcfstack(handles);
          user=questdlg('old file was loaded, we try to convert it to new format, you should check the fields and run a recalculation','WARNING ! !','OK','cancel and exit','default');   
        end
    catch
        % --- Show RCF stack here
        show_rcfstack(handles);
        user=questdlg('old file was loaded, we try to convert it to new format, you should check the fields and run a recalculation','WARNING ! !','OK','cancel and exit','default');     
    end              

if strcmp(user,'OK')
    cla('reset');
    set(handles.save_pushbutton,'Enable','Off');
    set(handles.print_pushbutton,'Enable','Off');
    set(handles.corroff,'Enable','Off');
    set(handles.corron,'Enable','Off');
    show_message(handles,'Plots not displayed, you should check parameters and recalculate to update the current file','red');
    
elseif strcmp(user,'loaded')   
    set(handles.save_pushbutton,'Enable','On');
    set(handles.print_pushbutton,'Enable','On');
    set(handles.corroff,'Enable','On');
    set(handles.corron,'Enable','On');    
    show_message(handles,'file loaded succesfuly...everything seems to be ok...','blue');
    
elseif user~=0
    close RCF_energyloss_GUI
end

%first try => very old files
catch
        questdlg('loaded file is too old to convert, please setup a new stack ! !','ERROR ! !','sad but true','default');
        
        set(handles.print_pushbutton,'Enable','Off');
        set(handles.corroff,'Enable','Off');
        set(handles.corron,'Enable','Off');
end
    
end

% --- Executes on button press in save_pushbutton.
function save_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to save_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.parameters.fversion=handles.progversion;

%check for data folder, if nonexistent create it
if ~exist('data', 'dir')
    mkdir('data')
end

pn = fullfile('data',handles.parameters.shotfilename);

if ~exist(pn, 'dir')
    mkdir(pn)
end

%%% Save
params = struct();
for lno = 1:8
    objHan = findobj('Tag', ['thick_L' num2str(lno) '_edit']);
    params.(['L' num2str(lno) '_thick']) = str2double(get(objHan, 'String'));
    objHan2 = findobj('Tag', ['mat_L' num2str(lno) '_edit']);
    objTags2 = get(objHan2, 'String');
    objVal2 = get(objHan2, 'Value');
    params.(['L' num2str(lno)]) = objTags2(objVal2);
end

clear lno objHan objHan2 objTags2 objVal2;

fn = [handles.parameters.shotfilename,'.mat'];
[fn,pn] = uiputfile(fn,'Save Workspace As', fullfile(pn,fn));
if ~isequal(fn,0)
    parameters = handles.parameters;
    parameters.params = params;
	save(fullfile(pn,fn), '-struct', 'parameters');
    clear parameters;
    
    fig=figure();
    show_picture(handles);
    saveshot=[handles.parameters.shotfilename,'.fig'];
    hgsave(fullfile(pn,saveshot));
    close(fig);
end
%%% End Save
guidata(hObject, handles);

% --- Executes on button press in print_pushbutton.
function print_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to print_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
colors=get(handles.axes1,'ColorOrder');
colors=[colors;colors;colors];
Edeposition = handles.parameters.Edeposition;

figure();
pos = get(gcf,'Position');
scrsz = get(0,'ScreenSize');
pos(3:4) = [scrsz(3)/2 scrsz(4)/2.5];
set(gcf, 'Position', pos)

hold on

%plot the data
j=0;
h1 = [];
legend_text = {};
col_index=1;
for i=1:length(handles.parameters.rcf_config)
	switch (char(handles.parameters.rcf_config(i)))
        case {'L1','L2','L3','L4','L5','L6','L7','L8'}
            % simply do nothing % disp('Absorption layer, no plot')
            j=j+1;
        case {'CR'}
            %plot front of CR39
            plot(handles.parameters.stoppingE(i,1),handles.parameters.stoppingE(i,2),'ro','MarkerSize',10, 'LineWidth', 2.0);
            h1(col_index) = plot(Edeposition(:,1,i),Edeposition(:,2,i),'-','Color',[colors(col_index,:)],'LineWidth',2);
            %plot back of CR39
            plot(handles.parameters.stoppingECR(i,1),handles.parameters.stoppingECR(i,2),'ro','MarkerSize',10, 'LineWidth', 2.0);
            h1(col_index) = plot(Edeposition(:,1,i),Edeposition(:,3,i),'-','Color',[colors(col_index,:)],'LineWidth',2);

            legend_text{col_index} = char([num2str(j,'%05.2d'),' - ',char(handles.parameters.rcf_config(i)),', ','E \approx ',num2str(handles.parameters.stoppingE(i),'%04.1f'),' MeV']);
            j=j+1;
            col_index=col_index+1;
        case {'HD','MD','M2','HS','H2','H3','E3','LA','ES','2H'}
            corroffhdl=findobj('Tag', 'corroff');
            corronhdl=findobj('Tag', 'corron');
            if (get(corronhdl,'Value')==1)
                plot(handles.parameters.stoppingE(i,1),handles.parameters.stoppingE(i,2),'ro','MarkerSize',10, 'LineWidth', 2.0);
                h1(col_index) = plot(Edeposition(:,1,i),Edeposition(:,4,i),'-','Color',[colors(col_index,:)],'LineWidth',2);
            elseif (get(corroffhdl,'Value')==1)
                plot(handles.parameters.stoppingE(i,1),handles.parameters.stoppingE(i,2),'ro','MarkerSize',10, 'LineWidth', 2.0);
                h1(col_index) = plot(Edeposition(:,1,i),Edeposition(:,2,i),'-','Color',[colors(col_index,:)],'LineWidth',2);
            else
                plot(handles.parameters.stoppingE(i,1),handles.parameters.stoppingE(i,2),'ro','MarkerSize',10, 'LineWidth', 2.0);
                h1(col_index) = plot(Edeposition(:,1,i),Edeposition(:,2,i),'-','Color',[colors(col_index,:)],'LineWidth',2);
            end
            legend_text{col_index} = char([num2str(j,'%05.2d'),' - ',char(handles.parameters.rcf_config(i)),', ','E \approx ',num2str(handles.parameters.stoppingE(i),'%04.1f'),' MeV']);
            j=j+1;
            col_index=col_index+1;
        otherwise
            set(handles.out_text,'String','Unknown layer.')
            break
	end
end


%run again with area plots for a better visibility
j=0;
col_index=1;
for i=1:length(handles.parameters.rcf_config)
	switch (char(handles.parameters.rcf_config(i)))
        case {'L1','L2','L3','L4','L5','L6','L7','L8'}
            % simply do nothing % disp('Absorption layer, no plot')
            j=j+1;
        case {'CR'}
            %plot front of CR39
            area(Edeposition(:,1,i),Edeposition(:,2,i),'FaceColor',[colors(col_index,:)],'EdgeColor',[colors(col_index,:)],'Linewidth',1)
            %plot back of CR39
            area(Edeposition(:,1,i),Edeposition(:,3,i),'FaceColor',[colors(col_index,:)],'EdgeColor',[colors(col_index,:)],'Linewidth',1)
            j=j+1;
            col_index=col_index+1;
        case {'HD','MD','M2','HS','LA', 'H2', 'H3', 'E3', 'ES', '2H'}
            area(Edeposition(:,1,i),Edeposition(:,2,i),'FaceColor',[colors(col_index,:)],'EdgeColor',[colors(col_index,:)],'Linewidth',1) %,'-','MarkerSize',5)
            j=j+1;
            col_index=col_index+1;
        otherwise
            set(handles.out_text,'String','Unknown layer.')
            break
	end
end
alpha(0.2)
hold off

protonhdl=findobj('Tag', 'proton_button');
if (get(corronhdl,'Value')==0)
	ylabel('Energy deposition (MeV)', 'FontSize', 16)
    if (get(protonhdl,'Value')==1)
        title('Energy deposition of protons in active layer', 'FontSize', 16)
    else
        title('Energy deposition of deuterons in active layer', 'FontSize', 16)
    end
    
else
    ylabel({'Energy deposition (shaded, MeV)'; 'Layer response function (lines, corr*MeV)'}, 'FontSize', 16)
    if (get(protonhdl,'Value')==1)
        title('Energy deposition and RCF response of protons in active layer', 'FontSize', 16)
    else
        title('Energy deposition and RCF response of deuterons in active layer', 'FontSize', 16)
    end
end

axis([0 max(max(Edeposition(:,1,:)))+0.1 0 ceil(max(max(Edeposition(:,2,:)))*10)/10+0.1])

xlabel('Kinetic energy E (MeV)', 'FontSize', 16)
set(gca,'Fontsize',14)
grid minor
legend(h1, legend_text, 'Fontsize', 12, 'Location','NorthEastOutside')

% --- Executes when selected object is changed in quench_panel.
function quench_panel_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in quench_panel 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
show_picture(handles);

% --- Executes when selected object is changed in speciespanel.
function speciespanel_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in speciespanel 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
current=get(eventdata.NewValue,'Tag');

if strcmp(current,'deut_button')  
    handles.parameters.ionspecies='deuterons';
else
    handles.parameters.ionspecies='protons';
end

set(handles.start_pushbutton,'Enable','Off');
pause(0.1);
guidata(hObject, handles);
update_species(handles);
set(handles.start_pushbutton,'Enable','On');

% --- Executes when selected object is changed in montecarlo.
function montecarlo_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in montecarlo 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
current=get(eventdata.NewValue,'Tag');

if strcmp(current,'mc_off')  
    handles.parameters.MC_parts=1;
else
    handles.parameters.MC_parts=handles.MC_parts;
end

guidata(hObject, handles);




%**************************************************************************
%
%  CALCULATION
%
%**************************************************************************
% --- Executes on button press in start_pushbutton.
function start_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to start_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.save_pushbutton,'Enable','Off');
set(handles.start_pushbutton,'Enable','Off');
set(handles.print_pushbutton,'Enable','Off');
set(handles.corroff,'Enable','Off');
set(handles.corron,'Enable','Off');

%disable proton/deuteron selection after first calculation
set(handles.proton_button,'Enable','Off');
set(handles.deut_button,'Enable','Off');

pause(0.5);

tic;

handles.parameters.fversion=handles.progversion;

% check for data folder, if nonexistent create it
if ~exist('data', 'dir')
    mkdir('data')
end
% check for shotfolder
pn = fullfile('data',handles.parameters.shotfilename);

if ~exist(pn, 'dir')
    mkdir(pn)
end

loss = getappdata(0, 'loss');

%convert in usable units
%start calculation for energy deposition
Emin = str2double(get(handles.emin_edit, 'String'))*1E3; %keV
Emax = str2double(get(handles.emax_edit, 'String'))*1E3; %keV
E_steps = str2double(get(handles.esteps_edit, 'String'))*1E3; %keV

index = ceil((Emax-Emin)/E_steps);
index2 = length(handles.parameters.rcf_config);
Edeposition = zeros(index,4,index2);
EdepositionCu = zeros(index,4,index2);

%if parallel toolbox - switch to java progress monitor
if handles.parallel
    progressStepSize = ceil(index/100);
    ppm = ParforProgMon('progress: ', index, progressStepSize, 400, 50); % Initialize process bar
end

%write params info before parfor; then you can access stuff in parfor
params = struct();

for lno = 1:8
    objHan = findobj('Tag', ['thick_L' num2str(lno) '_edit']);
    params.(['L' num2str(lno) '_thick']) = str2double(get(objHan, 'String'));
    objHan2 = findobj('Tag', ['mat_L' num2str(lno) '_edit']);
    objTags2 = get(objHan2, 'String');
    objVal2 = get(objHan2, 'Value');
    params.(['L' num2str(lno)]) = objTags2(objVal2);
end

clear lno objHan objHan2 objTags2 objVal2;

if strcmp(handles.parameters.ionspecies,'deuterons')
    ion='D';
    projmass=2.0;
else
    ion='H';
    projmass=1.0;
end

MCparticles=handles.parameters.MC_parts;

if ~handles.parallel
%if not parallel - print energy-progress into GUI; use for
   for k=1:index
       v=zeros(4,index2);
       vcu=zeros(4,index2);
       E=(k-1)*E_steps+Emin;
       E_new=E*ones(MCparticles,1);
       
       str=['Current energy: ',num2str(E/1E3,'%5.1f'),' MeV'];
       set(handles.out_text,'String',str);
       drawnow;
       
        for i=1:index2
          switch (char(handles.parameters.rcf_config(i)))
              case{'L1', 'L2', 'L3', 'L4', 'L5', 'L6', 'L7', 'L8'}
                  string = char(handles.parameters.rcf_config(i));
                  lno = str2double(string(2));                
                
                  if ~strcmpi(params.(['L' num2str(lno)]){:}, '--- select ---')
                    mat=params.(['L' num2str(lno)]){:};
                    tmp = elo(E_new, params.(['L' num2str(lno) '_thick']), loss.(mat).x, loss.(mat).c, loss.(mat).crange, loss.(mat).xrangeinv, loss.(mat).crangeinv, loss.(mat).k, loss.(mat).Nele, loss.(mat).Zeff, loss.(mat).Ieff, projmass, 0);
                    %preparation for NAIS => first row is starting energy
                    %second row is mean energy of proton before entering layer Lx
                    %third row is mean energy of proton after exiting layer Lx
                    %4th row is mean energloss in layer Lx
                    E_in = mean(E_new);
                    E_new = tmp(:,1);
                    E_stop = mean(tmp(:,2));
                    vcu(1,i) = E;
                    vcu(2,i) = E_in;
                    vcu(3,i) = mean(E_new);
                    vcu(4,i) = E_stop;
                  end
              case 'HD'
                 [E_new, E_stop, E_stop_corr] = elo_HD810(E_new, loss.([ion '_in_Gel']), loss.([ion '_in_Active']), loss.([ion '_in_Poly']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop;
                 v(4,i) = E_stop_corr;
              case 'MD'
                 [E_new, E_stop, E_stop_corr] = elo_MD55(E_new, loss.([ion '_in_Poly']), loss.([ion '_in_Active']), loss.([ion '_in_Adhesive']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop;
                 v(4,i) = E_stop_corr;
              case 'M2'
                 [E_new, E_stop, E_stop_corr] = elo_MD55V2(E_new, loss.([ion '_in_Poly']), loss.([ion '_in_Active']), loss.([ion '_in_Adhesive']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop;                 
                 v(4,i) = E_stop_corr;                 
              case 'HS'
                 [E_new, E_stop, E_stop_corr] = elo_HS(E_new, loss.([ion '_in_Poly']), loss.([ion '_in_Active']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop;
                 v(4,i) = E_stop_corr;
              case 'CR'
                 [E_new, E_stop_front, E_stop_back] = elo_CR39(E_new, loss.([ion '_in_CR39']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop_front;
                 v(3,i) = E_stop_back;
              case 'LA' %LANEX
                [E_new, E_stop] = elo_LANEX(E_new, loss.([ion '_in_LANEX']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop;
              case {'H2', 'H3'}
                 [E_new, E_stop, E_stop_corr] = elo_HD810v2(E_new, loss.([ion '_in_Active']), loss.([ion '_in_Poly']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop;
                 v(4,i) = E_stop_corr;
             case '2H'
                 [E_new, E_stop, E_stop_corr] = elo_DHv2(E_new, loss.([ion '_in_Active']), loss.([ion '_in_Poly']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop;
                 v(4,i) = E_stop_corr;
              case 'E3'
                 [E_new, E_stop, E_stop_corr] = elo_EBT3(E_new, loss.([ion '_in_Poly']), loss.([ion '_in_ActiveEBT3']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop;
                 v(4,i) = E_stop_corr;
              case 'ES'
                 [E_new, E_stop, E_stop_corr] = elo_EBT3half(E_new, loss.([ion '_in_Poly']), loss.([ion '_in_ActiveEBT3']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop;
                 v(4,i) = E_stop_corr;
              case 'H4'
                 [E_new, E_stop, E_stop_corr] = elo_HDv2(E_new, loss.([ion '_in_Active']), loss.([ion '_in_Poly']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop;
                 v(4,i) = E_stop_corr;
              otherwise
                 set(handles.out_text,'String','Unknown layer.')
                 break
          end
       end
       Edeposition(k,:,:)=v(:,:);
       EdepositionCu(k,:,:)=vcu(:,:);
   end
   
elseif handles.parallel
%if parallel - print energy-progress into progress bar; use parfor
   parfor k=1:index
       v=zeros(4,index2);
       vcu=zeros(4,index2);
       E=(k-1)*E_steps+Emin;
       E_new=E*ones(MCparticles,1);
     
        for i=1:index2
          switch (char(handles.parameters.rcf_config(i)))
              case{'L1', 'L2', 'L3', 'L4', 'L5', 'L6', 'L7', 'L8'}
                  string = char(handles.parameters.rcf_config(i));
                  lno = str2double(string(2));                
                
                  if ~strcmpi(params.(['L' num2str(lno)]){:}, '--- select ---')
                    mat=params.(['L' num2str(lno)]){:};
                    tmp = elo(E_new, params.(['L' num2str(lno) '_thick']), loss.(mat).x, loss.(mat).c, loss.(mat).crange, loss.(mat).xrangeinv, loss.(mat).crangeinv, loss.(mat).k, loss.(mat).Nele, loss.(mat).Zeff, loss.(mat).Ieff, projmass, 0);
                    %preparation for NAIS => first row is starting energy
                    %second row is mean energy of proton before entering layer Lx
                    %third row is mean energy of proton after exiting layer Lx
                    %4th row is mean energloss in layer Lx
                    E_in = mean(E_new);
                    E_new = tmp(:,1);
                    E_stop = mean(tmp(:,2));
                    vcu(1,i) = E;
                    vcu(2,i) = E_in;
                    vcu(3,i) = mean(E_new);
                    vcu(4,i) = E_stop;
                  end
              case 'HD'
                 [E_new, E_stop, E_stop_corr] = elo_HD810(E_new, loss.([ion '_in_Gel']), loss.([ion '_in_Active']), loss.([ion '_in_Poly']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop;
                 v(4,i) = E_stop_corr;
              case 'MD'
                 [E_new, E_stop, E_stop_corr] = elo_MD55(E_new, loss.([ion '_in_Poly']), loss.([ion '_in_Active']), loss.([ion '_in_Adhesive']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop;
                 v(4,i) = E_stop_corr;
              case 'M2'
                 [E_new, E_stop, E_stop_corr] = elo_MD55V2(E_new, loss.([ion '_in_Poly']), loss.([ion '_in_Active']), loss.([ion '_in_Adhesive']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop;                 
                 v(4,i) = E_stop_corr;                 
              case 'HS'
                 [E_new, E_stop, E_stop_corr] = elo_HS(E_new, loss.([ion '_in_Poly']), loss.([ion '_in_Active']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop;
                 v(4,i) = E_stop_corr;
              case 'CR'
                 [E_new, E_stop_front, E_stop_back] = elo_CR39(E_new, loss.([ion '_in_CR39']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop_front;
                 v(3,i) = E_stop_back;
              case 'LA' %LANEX
                [E_new, E_stop] = elo_LANEX(E_new, loss.([ion '_in_LANEX']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop;
              case {'H2', 'H3'}
                 [E_new, E_stop, E_stop_corr] = elo_HD810v2(E_new, loss.([ion '_in_Active']), loss.([ion '_in_Poly']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop;
                 v(4,i) = E_stop_corr;
             case '2H'
                 [E_new, E_stop, E_stop_corr] = elo_DHv2(E_new, loss.([ion '_in_Active']), loss.([ion '_in_Poly']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop;
                 v(4,i) = E_stop_corr;
              case 'E3'
                 [E_new, E_stop, E_stop_corr] = elo_EBT3(E_new, loss.([ion '_in_Poly']), loss.([ion '_in_ActiveEBT3']), projmass);
                 v(1,i) = E;
                 v(2,i) = E_stop;
                 v(4,i) = E_stop_corr;
              otherwise
                 set(handles.out_text,'String','Unknown layer.')
                 break
          end
       end
       Edeposition(k,:,:)=v(:,:);
       EdepositionCu(k,:,:)=vcu(:,:);

       %progressbar increment
       if mod(k,progressStepSize)==0
           ppm.increment();
       end
   end
   
end %end of case parallel/non-parallel toolbox
%workarround, cause matlab jumbs for non-parallel toolbox versions if "if
%statement" is in parfor loop into this if-loop => BUG
    
clear v vcu;

if handles.parallel
    ppm.delete() %delete progressbar
end

%convert back to MeV
Edeposition(:,:,:)=Edeposition.*1E-3;
EdepositionCu(:,:,:)=EdepositionCu.*1E-3;

handles.parameters.stoppingE=zeros(size(Edeposition,3),2);
stoppingE=zeros(size(Edeposition,3),2);

for i=1:size(Edeposition,3)
    X=Edeposition(:,1,i);
    Y=Edeposition(:,2,i);
    [a, ind] = max(Y);
    stoppingE(i,1) = X(ind);
    stoppingE(i,2) = a;
end

handles.parameters.stoppingECR=zeros(size(Edeposition,3),2);
stoppingECR=zeros(size(Edeposition,3),2);
%backside CR-39
for i=1:size(Edeposition,3)
    X=Edeposition(:,1,i);
    Y=Edeposition(:,3,i);
    [a, ind] = max(Y);
    stoppingECR(i,1) = X(ind);
    stoppingECR(i,2) = a;
end

handles.parameters.stoppingE(:,:) = stoppingE(:,:);
handles.parameters.stoppingECR(:,:) = stoppingECR(:,:);
clear a ind X Y stoppingE stoppingECR;

handles.parameters.Edeposition=Edeposition;
handles.parameters.EdepositionCu=EdepositionCu;

show_picture(handles);

%%% Save
params = struct();
for lno = 1:8
    objHan = findobj('Tag', ['thick_L' num2str(lno) '_edit']);
    params.(['L' num2str(lno) '_thick']) = str2double(get(objHan, 'String'));
    objHan2 = findobj('Tag', ['mat_L' num2str(lno) '_edit']);
    objTags2 = get(objHan2, 'String');
    objVal2 = get(objHan2, 'Value');
    params.(['L' num2str(lno)]) = objTags2(objVal2);
end

clear lno objHan objHan2 objTags2 objVal2;

fn = [handles.parameters.shotfilename,'.mat'];
parameters = handles.parameters;
parameters.params = params;
save(fullfile(pn,fn), '-struct', 'parameters');
clear parameters;

fig=figure();
show_picture(handles);
saveshot=[handles.parameters.shotfilename,'.fig'];
hgsave(fullfile(pn,saveshot));
close(fig);
%%% End Save

time=toc/60;
set(handles.out_text,'String',['Time for calculation: ',num2str(time,'%5.1f'),' min.']);

set(handles.save_pushbutton,'Enable','On');
set(handles.start_pushbutton,'Enable','On');
set(handles.print_pushbutton,'Enable','On');
set(handles.corroff,'Enable','On');
set(handles.corron,'Enable','On');

% Update handles structure
guidata(hObject, handles);


%***********************************************************
%
% function show_rcfstack
%
%***********************************************************
%--- show RCF stack
function show_rcfstack(handles)

% Get the current position of the GUI from the handles structure
set(handles.stack_axes,'Visible', 'off', 'Units', 'pixels')
stack_axes_size = get(handles.stack_axes,'Position');
%set to active axes
axes(handles.stack_axes);
%construct image
imgfile = ['icons/' 'line' '.png'];
img = imread(imgfile);
imginfo = imfinfo(imgfile);
factor=stack_axes_size(4)/imginfo.Height;
image=imresize(img,factor);
handles.images.stack=image;
mStackPlain = '';

for i=1:length(handles.parameters.rcf_config)
    % new realname layer table
    mName = '';
    mThick = '';
    switch (char(handles.parameters.rcf_config(i)))
        case{'L1', 'L2', 'L3', 'L4', 'L5', 'L6', 'L7', 'L8'}
            string = char(handles.parameters.rcf_config(i));
            lno = str2double(string(2));
            objHan = findobj('Tag', ['mat_L' num2str(lno) '_edit']);
            objTags = get(objHan, 'String');
            objVal = get(objHan, 'Value');
            file = char(objTags(objVal));
            if ~strcmpi(file, '--- select ---')
                imgfile = ['icons/' file(6:end) '.png'];
                % name
                mName = file(6:7);
                %thick
                objHan2 = findobj('Tag', ['thick_L' num2str(lno) '_edit']);
                mThick = get(objHan2, 'String');
            else
                imgfile = ['icons/' 'line' '.png'];
            end
        case 'HD'
            imgfile = ['icons/' 'hd810' '.png'];
            mName = 'HD';
        case 'MD'
            imgfile = ['icons/' 'md55' '.png'];
            mName = 'MD';
        case 'M2'
            imgfile = ['icons/' 'md55v2' '.png'];
            mName = 'M2';
        case 'HS'
            imgfile = ['icons/' 'hs' '.png'];
            mName = 'HS';
        case 'CR'
            imgfile = ['icons/' 'cr39' '.png'];
            mName = 'CR';
        case 'LA'
            imgfile = ['icons/' 'lanex' '.png'];
            mName = 'LA';
        case 'H2'
            imgfile = ['icons/' 'hd810v2' '.png'];
            mName = 'H2';
        case 'H3'
            imgfile = ['icons/' 'hd810v3' '.png'];
            mName = 'H3';
        case '2H'
            imgfile = ['icons/' 'dhv2' '.png'];
            mName = '2H';
        case 'E3'
            imgfile = ['icons/' 'ebt3' '.png'];
            mName = 'E3';
        case 'ES'
            imgfile = ['icons/' 'ebt3half' '.png'];
            mName = 'ES';
        case 'H4'
            imgfile = ['icons/' 'hdv4' '.png'];
            mName = 'H4';
        otherwise
            set(handles.out_text,'String','Unknown layer.')
            break
    end
    try 
        img = imread(imgfile);
    catch
        imgfile = ['icons/' 'blank' '.png']; 
        img = imread(imgfile);
    end
    imginfo = imfinfo(imgfile);
    factor = stack_axes_size(4)/imginfo.Height;
    image = imresize(img,factor);
    handles.images.stack = horzcat(handles.images.stack, image);
    mStackPlain = horzcat(mStackPlain, [mName mThick ';']);
end

set(handles.RCF_config_plain_edit,'String', mStackPlain);
imagesc(handles.images.stack)
stack_size=size(handles.images.stack);
if (stack_size(1) > stack_axes_size(3))
    factor=stack_axes_size(3)/stack_size(1);
    handles.images.stack=imresize(handles.images.stack,factor);
    stack_size=size(handles.images.stack);
    y=0.5*(stack_axes_size(4)-stack_size(1));
    set(handles.stack_axes, 'Visible', 'off','Units', 'pixels', ...
    'Position', [10 2+y stack_size(2) stack_size(1)]);
else
%     factor=stack_axes_size(3)/stack_size(2);
%     handles.images.stack=imresize(handles.images.stack,factor);
%     y=0.5*(stack_axes_size(4)-stack_size(1));
    set(handles.stack_axes, 'Visible', 'off','Units', 'pixels'); %, ...
%     'Position', [10 2+y stack_size(2) stack_size(1)]);
end

axes(handles.axes1);
% Update handles structure
hObject=handles.output;
guidata(hObject, handles);


%***********************************************************************
%
% --- show picture and save data files
%
%***********************************************************************
function h=show_picture(handles)

% check for data folder, if nonexistent create it
if ~exist('data', 'dir')
    mkdir('data')
end
% check for shotfolder
pn = fullfile('data',handles.parameters.shotfilename);

if ~exist(pn, 'dir')
    mkdir(pn)
else
    % clear folder from old *.dat files
    delete([pn filesep '*.dat']);
end

if isfield(handles.parameters, 'Edeposition')
    Edeposition = handles.parameters.Edeposition;
    EdepositionCu = handles.parameters.EdepositionCu;
else
    Edeposition = zeros(2,2,length(handles.parameters.rcf_config));
    EdepositionCu = zeros(2,2,length(handles.parameters.rcf_config));
end

%update axes
axes(handles.axes1);
cla;
hold all

%plot the data
j = 1; f = 1;
for i=1:length(handles.parameters.rcf_config)
    clear data;
	switch (char(handles.parameters.rcf_config(i)))
        case {'L1','L2','L3','L4','L5','L6','L7','L8'}
            % simply do nothing
            % disp('Absorption layer, no plot')
            filename=fullfile(pn, [handles.parameters.shotfilename,'_',num2str(j,'%05.2d'),'_',char(handles.parameters.rcf_config(i)),'.dat']);
            data(:,:)=EdepositionCu(:,:,i);
            save(filename,'data', '-ascii', '-double') ;
            j=j+1;
        case {'CR'}
            %plot front of CR39
            h = plot(Edeposition(:,1,i),Edeposition(:,2,i),'-o','MarkerSize',5);
            h1=plot(handles.parameters.stoppingE(i,1),handles.parameters.stoppingE(i,2),'ro','MarkerSize',10, 'LineWidth', 2.0);
            hasbehavior(h1,'legend',false);
            h.DisplayName = char([num2str(j,'%05.2d'),'|',num2str(f,'%05.2d'),' - ',char(handles.parameters.rcf_config(i)),'-front, ','E \approx ',num2str(handles.parameters.stoppingE(i),'%04.1f'),' MeV']);
            filename=fullfile(pn, [handles.parameters.shotfilename,'_',num2str(j,'%05.2d'),'_',char(handles.parameters.rcf_config(i)),'_front.dat']);
            data(:,:)=Edeposition(:,:,i);
            save(filename,'data', '-ascii', '-double');

            %plot back of CR39
            h = plot(Edeposition(:,1,i),Edeposition(:,3,i),'-o','MarkerSize',5);
            h1=plot(handles.parameters.stoppingECR(i,1),handles.parameters.stoppingECR(i,2),'ro','MarkerSize',10, 'LineWidth', 2.0);
            hasbehavior(h1,'legend',false);
            h.DisplayName = char([num2str(j,'%05.2d'),'|',num2str(f,'%05.2d'),' - ',char(handles.parameters.rcf_config(i)),'-back, ','E \approx ',num2str(handles.parameters.stoppingECR(i),'%04.1f'),' MeV']);
            filename=fullfile(pn,[handles.parameters.shotfilename,'_',num2str(j,'%05.2d'),'_',char(handles.parameters.rcf_config(i)),'_back.dat']);
            data(:,:)=Edeposition(:,:,i);
            save(filename,'data', '-ascii', '-double');
            j=j+1; f=f+1;
        case {'HD','MD','M2','HS','H2','H3','E3','ES','H4','2H'}
            corroffhdl=findobj('Tag', 'corroff');
            corronhdl=findobj('Tag', 'corron');
            h1=plot(handles.parameters.stoppingE(i,1),handles.parameters.stoppingE(i,2),'ro','MarkerSize',10, 'LineWidth', 2.0);
            hasbehavior(h1,'legend',false);
            if (get(corronhdl,'Value')==1)
                h = plot(Edeposition(:,1,i),Edeposition(:,4,i),'-x','MarkerSize',5);
            elseif (get(corroffhdl,'Value')==1)
                h = plot(Edeposition(:,1,i),Edeposition(:,2,i),'-o','MarkerSize',5);             
            else
                h = plot(Edeposition(:,1,i),Edeposition(:,2,i),'-o','MarkerSize',5);
            end
            h.DisplayName = char([num2str(j,'%05.2d'),'|',num2str(f,'%05.2d'),' - ',char(handles.parameters.rcf_config(i)),', ','E \approx ',num2str(handles.parameters.stoppingE(i),'%04.1f'),' MeV']);
            filename=fullfile(pn,[handles.parameters.shotfilename,'_',num2str(j,'%05.2d'),'_',char(handles.parameters.rcf_config(i)),'.dat']);
            data(:,:)=Edeposition(:,:,i);
            save(filename,'data', '-ascii', '-double');
            j=j+1; f=f+1;
        case {'LA'}
            h = plot(Edeposition(:,1,i),Edeposition(:,2,i),'-o','MarkerSize',5);
            h1=plot(handles.parameters.stoppingE(i,1),handles.parameters.stoppingE(i,2),'ro','MarkerSize',10, 'LineWidth', 2.0);
            hasbehavior(h1,'legend',false);
            h.DisplayName = char([num2str(j,'%05.2d'),'|',num2str(f,'%05.2d'),' - ',char(handles.parameters.rcf_config(i)),', ','E \approx ',num2str(handles.parameters.stoppingE(i),'%04.1f'),' MeV']);
            filename=fullfile(pn,[handles.parameters.shotfilename,'_',num2str(j,'%05.2d'),'_',char(handles.parameters.rcf_config(i)),'.dat']);
            data(:,:)=Edeposition(:,:,i);
            save(filename,'data', '-ascii', '-double');
            j=j+1; f=f+1;
        otherwise
            h = plot(1,1);
            set(handles.out_text,'String','Unknown layer.')
            corroffhdl=findobj('Tag', 'corroff');
            corronhdl=findobj('Tag', 'corron');
            h.DisplayName = char([num2str(j,'%05.2d'),'|',num2str(f,'%05.2d'),' - ','Unknown layer']);
            break
	end
end
hold off
xlabel('Kinetic energy E (MeV)', 'FontWeight', 'bold', 'FontSize', 12)

protonhdl=findobj('Tag', 'proton_button');

if (get(corronhdl,'Value')==0)
    ylabel('Energy deposition (MeV)', 'FontWeight', 'bold', 'FontSize', 12)
    if (get(protonhdl,'Value')==1)
        title('Energy deposition of protons in active layer', 'FontWeight', 'bold', 'FontSize', 16)
    else
        title('Energy deposition of deuterons in active layer', 'FontWeight', 'bold', 'FontSize', 16)
    end
    
else
    ylabel('Active layer response function (cor*MeV)', 'FontWeight', 'bold', 'FontSize', 12)
    if (get(protonhdl,'Value')==1)
        title('RCF response of protons in active layer', 'FontWeight', 'bold', 'FontSize', 16)
    else
        title('RCF response of deuterons in active layer', 'FontWeight', 'bold', 'FontSize', 16)
    end
end

box on;
legend('Location','NorthWestOutside');

%**************************************************************************
%
%  update_species - helper function
%
%**************************************************************************
function update_species(handles)

% Layer edit popup menu items
pop_items = {'--- select ---'};
a = dir(['srimfiles/',handles.parameters.ionspecies]);
for i=1:length(a)
    if ~(a(i).isdir)
        [~, name] = fileparts(a(i).name);
        pop_items = [pop_items; name];
    end
end

try
    rmappdata(0, 'loss');
end

%even tables reads H_in_active; loss.ionspecies gives information about the ion
%species for corresponding loss file => can be done better, but now easier
%to implement
loss.ionspecies=handles.parameters.ionspecies;

if strcmp(handles.parameters.ionspecies,'deuterons')
    loss.D_in_Active = srimread([['srimfiles/',handles.parameters.ionspecies] filesep 'films' filesep 'D in Active-Layer'],handles,1);
    loss.D_in_ActiveEBT3 = srimread([['srimfiles/',handles.parameters.ionspecies] filesep 'films' filesep 'EBT3 D in Active-Layer'],handles,1);
    loss.D_in_Poly = srimread([['srimfiles/',handles.parameters.ionspecies] filesep 'films' filesep 'D in Polyester'],handles);
    loss.D_in_Gel = srimread([['srimfiles/',handles.parameters.ionspecies] filesep 'films' filesep 'HD-810v1 D in Gelatin'],handles);
    loss.D_in_CR39 = srimread([['srimfiles/',handles.parameters.ionspecies] filesep 'films' filesep 'D in CR-39'],handles);
    loss.D_in_LANEX = srimread([['srimfiles/',handles.parameters.ionspecies] filesep 'films' filesep 'D in Lanex'],handles);
    loss.D_in_Adhesive = srimread([['srimfiles/',handles.parameters.ionspecies] filesep 'films' filesep 'MD-55 D in acrylic adhesive'],handles);
else
    loss.H_in_Active = srimread([['srimfiles/',handles.parameters.ionspecies] filesep 'films' filesep 'H in Active-Layer'],handles,1);
    loss.H_in_ActiveEBT3 = srimread([['srimfiles/',handles.parameters.ionspecies] filesep 'films' filesep 'EBT3 H in Active-Layer'],handles,1);
    loss.H_in_Poly = srimread([['srimfiles/',handles.parameters.ionspecies] filesep 'films' filesep 'H in Polyester'],handles);
    loss.H_in_Gel = srimread([['srimfiles/',handles.parameters.ionspecies] filesep 'films' filesep 'HD-810v1 H in Gelatin'],handles);
    loss.H_in_CR39 = srimread([['srimfiles/',handles.parameters.ionspecies] filesep 'films' filesep 'H in CR-39'],handles);
    loss.H_in_LANEX = srimread([['srimfiles/',handles.parameters.ionspecies] filesep 'films' filesep 'H in Lanex'],handles);
    loss.H_in_Adhesive = srimread([['srimfiles/',handles.parameters.ionspecies] filesep 'films' filesep 'MD-55 H in acrylic adhesive'],handles);
end

for i=2:length(pop_items)
    loss.(pop_items{i}) = srimread([['srimfiles/',handles.parameters.ionspecies] filesep pop_items{i}],handles);
end

setappdata(0, 'loss', loss);

set(handles.mat_L1_edit, 'String', pop_items);
set(handles.mat_L2_edit, 'String', pop_items);
set(handles.mat_L3_edit, 'String', pop_items);
set(handles.mat_L4_edit, 'String', pop_items);
set(handles.mat_L5_edit, 'String', pop_items);
set(handles.mat_L6_edit, 'String', pop_items);
set(handles.mat_L7_edit, 'String', pop_items);
set(handles.mat_L8_edit, 'String', pop_items);

set(handles.emin_edit,'String', 0.5);
set(handles.emax_edit,'String', 30);
set(handles.esteps_edit,'String', 0.1);
set(handles.thick_L1_edit,'String', 0);
set(handles.thick_L2_edit,'String', 0);
set(handles.thick_L3_edit,'String', 0);
set(handles.thick_L4_edit,'String', 0);
set(handles.thick_L5_edit,'String', 0);
set(handles.thick_L6_edit,'String', 0);
set(handles.thick_L7_edit,'String', 0);
set(handles.thick_L8_edit,'String', 0);
set(handles.mat_L1_edit, 'Value', 1);
set(handles.mat_L2_edit, 'Value', 1);
set(handles.mat_L3_edit, 'Value', 1);
set(handles.mat_L4_edit, 'Value', 1);
set(handles.mat_L5_edit, 'Value', 1);
set(handles.mat_L6_edit, 'Value', 1);
set(handles.mat_L7_edit, 'Value', 1);
set(handles.mat_L8_edit, 'Value', 1);


%**************************************************************************
%
%  show_msg - helper function
%
%**************************************************************************
function show_message(handles,text,color)
set(handles.out_text,'String',text,'ForegroundColor',color);
pause(5.0)
set(handles.out_text,'String','','ForegroundColor','black');

