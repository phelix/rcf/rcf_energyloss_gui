# RCF Energyloss GUI

RCF_EnergyLoss_GUI Matlab routines for calculating energy loss of protons/deuterons in given materials.

## License and copyright information
### License

RCF Spectrum GUI is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
long with this program.  If not, see <https://www.gnu.org/licenses/>.

### Copyright owners

RCF Spectrum GUI contributors, including those listed in the CREDITS file, hold the
copyright to this work.

### Additional license information

Some components of RCF Spectrum GUI imported from other projects may be under other
Free and Open Source, or Free Culture, licenses. Specific details of their
licensing information can be found in those components.

## Getting Started

Einfach jemand fragen der sich damit auskennt.

## zur Zeit sind folgende RCF eingerichtet

Die Liste ist stets im [PPH Wiki](http://pphwiki.gsi.de/wiki/RCF) (nur GSI intern) zu führen. Hier nur händisch kopiert.

* **HD**; 0,75 µm Gel, 6,5 µm aktive, 96,52 µm Poly
* **MD**; 67,31 µm Poly, 16 µm aktive, 20 µm Kleber, 25 µm Poly, 20 µm Kleber, 16 µm aktive, 67,31 µm Poly
* **M2**; 96 µm Poly, 17,5 µm aktive, 20 µm Kleber, 25 µm Poly, 20 µm Kleber, 17,5 µm aktive, 96 µm Poly
* **HS**; 96,53 µm Poly, 40 µm aktive, 96,53 µm Poly
* **H2**; **H3**; 8 µm aktive, 96 µm Poly
* **2H**; 96 µm Poly, 8 µm aktive (H2 anders rum)
* **E3**; 125 µm Poly, 28 µm aktive, 125 µm Poly
* **ES**; 28 µm aktive, 125 µm Poly
* **H4**; 12 µm aktive, 97 µm Poly
* **CR**; Routine to calculate the energyloss and energydeposition in 1.1 mm CR-39
* **LA**; Routine to calculate the energyloss and energydeposition in LANEX Regular
