function [E_new, E_stop_front, E_stop_back] = elo_CR39(E_new, H_in_CR39, projmass)
% Routine to calculate the energyloss and energydeposition in 1.1 mm CR-39

% Copyright (C) 2021 by it's contributors.
% Some rights reserved. See COPYING, CREDITS.
%
% This file is part of RCF Energyloss GUI.
%
% RCF Energyloss GUI is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% long with this program.  If not, see <https://www.gnu.org/licenses/>.

thickness_front = 100; % um
thickness_middle = 900; % um
thickness_back = 100; % um

tmp = elo(E_new, thickness_front, H_in_CR39.x, H_in_CR39.c, H_in_CR39.crange, H_in_CR39.xrangeinv, H_in_CR39.crangeinv, H_in_CR39.k, H_in_CR39.Nele, H_in_CR39.Zeff, H_in_CR39.Ieff, projmass, 0);
E_new = tmp(:,1);
E_stop_front = mean(tmp(:,2));

tmp = elo(E_new, thickness_middle, H_in_CR39.x, H_in_CR39.c, H_in_CR39.crange, H_in_CR39.xrangeinv, H_in_CR39.crangeinv, H_in_CR39.k, H_in_CR39.Nele, H_in_CR39.Zeff, H_in_CR39.Ieff, projmass, 0);
E_new = tmp(:,1);

tmp = elo(E_new, thickness_back, H_in_CR39.x, H_in_CR39.c, H_in_CR39.crange, H_in_CR39.xrangeinv, H_in_CR39.crangeinv, H_in_CR39.k, H_in_CR39.Nele, H_in_CR39.Zeff, H_in_CR39.Ieff, projmass, 0);
E_new = tmp(:,1);
E_stop_back = mean(tmp(:,2));
end