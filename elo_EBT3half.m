function [E_new, E_stop, E_stop_corr] = elo_EBT3half(E_new, H_in_Poly, H_in_ActiveEBT3, projmass)
% Routine to calculate the energyloss and energydeposition in EBT3

% Copyright (C) 2021 by it's contributors.
% Some rights reserved. See COPYING, CREDITS.
%
% This file is part of RCF Energyloss GUI.
%
% RCF Energyloss GUI is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% long with this program.  If not, see <https://www.gnu.org/licenses/>.

thickness_poly_ebt3 = 125; % microns
thickness_active_ebt3 = 28; % microns

%assume same correction like active layer in HD => needs to be checked
%experimentaly
tmp = elo(E_new, thickness_active_ebt3, H_in_ActiveEBT3.x, H_in_ActiveEBT3.c, H_in_ActiveEBT3.crange, H_in_ActiveEBT3.xrangeinv, H_in_ActiveEBT3.crangeinv, H_in_ActiveEBT3.k, H_in_ActiveEBT3.Nele, H_in_ActiveEBT3.Zeff, H_in_ActiveEBT3.Ieff, projmass, 1, H_in_ActiveEBT3.ccorr);
E_new = tmp(:,1);
E_stop = mean(tmp(:,2));
E_stop_corr = mean(tmp(:,3));

tmp = elo(E_new, thickness_poly_ebt3, H_in_Poly.x, H_in_Poly.c, H_in_Poly.crange, H_in_Poly.xrangeinv, H_in_Poly.crangeinv, H_in_Poly.k, H_in_Poly.Nele, H_in_Poly.Zeff, H_in_Poly.Ieff, projmass, 0);
E_new = tmp(:,1);
end
