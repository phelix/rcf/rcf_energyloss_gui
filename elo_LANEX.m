function [E_new, E_stop] = elo_LANEX(E_new, H_in_LANEX, projmass)
% Routine to calculate the energyloss and energydeposition in LANEX Regular
% calculates stopping in total LANEX regular equivalent SiN
%LANEX 12 mils thick .3048 mm(measured with caliper)  
%LANEX fine @ effect density of ~3.1 g/cm^3
%544nm light peak, Gd2O2S:Tb, 7.3 g/cm^3 effective thickness= 33.7 mg/cm^2 Chapter3 and 4 of University
%of Grongingen NL, http://dissertations.ub.rug.nl/FILES/faculties/science/1998/s.n.boon/

% Copyright (C) 2021 by it's contributors.
% Some rights reserved. See COPYING, CREDITS.
%
% This file is part of RCF Energyloss GUI.
%
% RCF Energyloss GUI is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% long with this program.  If not, see <https://www.gnu.org/licenses/>.

thickness_LANEX = 324.8;% active Gd_2 O_2 S:Tb layer is measured to be 324.8 um thick with polystyrene backing 44.8 um (plastic side was away from stack)

tmp = elo(E_new, thickness_LANEX, H_in_LANEX.x, H_in_LANEX.c, H_in_LANEX.crange, H_in_LANEX.xrangeinv, H_in_LANEX.crangeinv, H_in_LANEX.k, H_in_LANEX.Nele, H_in_LANEX.Zeff, H_in_LANEX.Ieff, projmass, 0);
E_new = tmp(:,1);
E_stop = mean(tmp(:,2)); 
end