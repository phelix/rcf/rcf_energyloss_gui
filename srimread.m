function [ output ] = srimread(file,handles,corr)
%SRIMREAD reads original srim outputfiles => gives back matrix energy,
%sumof Eloss_elec and Eloss_nuclear 

% Copyright (C) 2013,2021 by it's contributors.
% Some rights reserved. See COPYING, CREDITS.
%
% This file is part of RCF Energyloss GUI.
%
% RCF Energyloss GUI is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% long with this program.  If not, see <https://www.gnu.org/licenses/>.

%disable correction table by default
if nargin<3
    corr=0;
end

%fit parameters for correction of active layer effectivness
%from detailed analysis
estbest=[-2.30973301002, 0.963644008787, 0.0916032981425, 0.0270710937845, 0.00727873373894, -2.49447117053];

%tabulated mean ionisation potentials (eV) from
%http://www.srim.org/SRIM/SRIMPICS/IONIZ.htm sorted by atomic number up to Bi(83); first
%row if element is in solid material, second if in gassous material
meanIoni=[ [19.4,19.6]; [41.6,39.0]; [42.2,42.2]; [63.7,63.7]; [76.0,76.0];
           [79.1,79.1]; [84.2,84.2]; [96.0,96.0]; [115.0,107.0]; [136.0,133.0];
           [149.0,149.0]; [156.0,156.0]; [170.0,170.0]; [171.0,171.0]; [173.0,173.0];
           [184.0,184.0]; [178.0,178.0]; [196.0,190.0]; [190.0,190.0]; [198.0,198.0];
           [225.0,225.0]; [231.0,231.0]; [249.0,249.0]; [265.0,265.0]; [275.0,275.0];
           [286.0,286.0]; [306.0,306.0]; [321.0,321.0]; [332.0,332.0]; [330.0,330.0];
           [334.0,334.0]; [335.0,335.0]; [353.0,353.0]; [351.0,351.0]; [343.0,317.0];
           [348.0,348.0]; [364.0,364.0]; [366.0,366.0]; [375.0,375.0]; [389.0,389.0];
           [417.0,417.0]; [423.0,423.0]; [428.0,428.0]; [441.0,441.0]; [468.0,468.0];
           [476.0,476.0]; [488.0,488.0]; [441.0,441.0]; [481.0,481.0]; [478.0,478.0];
           [472.0,472.0]; [485.0,485.0]; [491.0,455.0]; [460.0,440.0]; [488.0,488.0];
           [491.0,491.0]; [472.0,472.0]; [501.0,501.0]; [513.0,513.0]; [546.0,546.0];
           [560.0,560.0]; [566.0,566.0]; [580.0,580.0]; [586.0,586.0]; [614.0,614.0];
           [603.0,603.0]; [641.0,641.0]; [661.0,661.0]; [683.0,683.0]; [684.0,684.0];
           [694.0,694.0]; [675.0,675.0]; [729.0,729.0]; [735.0,735.0]; [749.0,749.0];
           [746.0,746.0]; [757.0,757.0]; [792.0,792.0]; [789.0,789.0]; [800.0,800.0];
           [810.0,810.0]; [787.0,787.0]; [819.0,819.0] ];

%max energy for which correction is 1 in keV
maxencorr=10.145962517400582*1000;

%start to read header information from file
fid = fopen(file, 'rt'); 
lines = textscan(fid,'%[^\n]'); %reads line by line 
fclose(fid); 

checker = find(~cellfun(@isempty,strfind(lines{:}, 'keV / micron')),1,'first');

endHeader = find(~cellfun(@isempty,strfind(lines{:}, '-----------  ---------- ---------- ----------  ----------  ----------')),1,'first'); 
startfeed = find(~cellfun(@isempty,strfind(lines{:}, '-----------------------------------------------------------')),1,'first'); 
output.table=zeros(startfeed-endHeader-2,2);
output.rangetable=zeros(startfeed-endHeader-2,2);
rangetab=cell(startfeed-endHeader-2,1);

unitstr=cell(startfeed-endHeader-2,1);

if strcmp(strtrim(lines{1}{checker}), 'Stopping Units =  keV / micron')
  j=1;
  for i=endHeader+1:startfeed-1
    index=findstr(lines{1}{i}, ' ');
    unitstr{j}=lines{1}{i}(index(1):index(2));

    %be compatible to current xls-approach; convert all energies to keV
    %ziegler bug; if unit=eV => there is one whitespace more than keV, MeV and GeV
    if strcmp(unitstr{j},' eV ')
      output.table(j,1)=str2num(strrep(lines{1}{i}(1:index(1)), ',', '.'))/1000;
      output.table(j,2)=str2num(strrep(lines{1}{i}(index(5):index(6)), ',', '.'))+str2num(strrep(lines{1}{i}(index(7):index(8)), ',', '.'));
      rangetab{j,1}=lines{1}{i}(index(8):index(8)+11);
    elseif strcmp(unitstr{j},' keV ')
      output.table(j,1)=str2num(strrep(lines{1}{i}(1:index(1)), ',', '.'));
      output.table(j,2)=str2num(strrep(lines{1}{i}(index(4):index(5)), ',', '.'))+str2num(strrep(lines{1}{i}(index(6):index(7)), ',', '.'));
      rangetab{j,1}=lines{1}{i}(index(7):index(7)+11);
    elseif strcmp(unitstr{j},' MeV ')
      output.table(j,1)=str2num(strrep(lines{1}{i}(1:index(1)), ',', '.'))*1000;
      output.table(j,2)=str2num(strrep(lines{1}{i}(index(4):index(5)), ',', '.'))+str2num(strrep(lines{1}{i}(index(6):index(7)), ',', '.'));
      rangetab{j,1}=lines{1}{i}(index(7):index(7)+11);
    elseif strcmp(unitstr{j},' GeV ')
      output.table(j,1)=str2num(strrep(lines{1}{i}(1:index(1)), ',', '.'))*1e6;
      output.table(j,2)=str2num(strrep(lines{1}{i}(index(4):index(5)), ',', '.'))+str2num(strrep(lines{1}{i}(index(6):index(7)), ',', '.'));
      rangetab{j,1}=lines{1}{i}(index(7):index(7)+11);
    end
 
    j=j+1;
  end
  
  %build rangetable in units of um and keV
  rangetab=strtrim(rangetab);
  rangetab=strrep(rangetab, ',', '.');
  
  for i=1:length(rangetab)
      tempstr=strtrim(rangetab{i}(end-1:end));
      index=findstr(rangetab{i}, ' ');
      if strcmp(tempstr,'A')
          output.rangetable(i,1)=output.table(i,1);
          output.rangetable(i,2)=1.0e-4*str2num(rangetab{i}(1:index));
      elseif strcmp(tempstr,'um')
          output.rangetable(i,1)=output.table(i,1);
          output.rangetable(i,2)=1.0*str2num(rangetab{i}(1:index));
      elseif strcmp(tempstr,'mm')
          output.rangetable(i,1)=output.table(i,1);
          output.rangetable(i,2)=1.0e3*str2num(rangetab{i}(1:index));
      elseif strcmp(tempstr,'m')
          output.rangetable(i,1)=output.table(i,1);
          output.rangetable(i,2)=1.0e6*str2num(rangetab{i}(1:index));
      elseif strcmp(tempstr,'km')
          output.rangetable(i,1)=output.table(i,1);
          output.rangetable(i,2)=1.0e9*str2num(rangetab{i}(1:index));
      end
  end
  
  %extract material properties like atomic %, particle density, charge
  %numbers etc. to calculate effective electron-density and effective
  %charge-density => we need this for the MC fluctuation model later...
  densityline = find(~cellfun(@isempty,strfind(lines{:}, 'Target Density')),1,'first');
  atomdensity = str2num(strrep(lines{1}{densityline}(38:47), ',','.'))/1000;
  
  startfeed = find(~cellfun(@isempty,strfind(lines{:}, '======= Target  Composition ========')),1,'first'); 
  endHeader = find(~cellfun(@isempty,strfind(lines{:}, '====================================')),3,'first');
  endHeader = endHeader(3);
  
  compoundtab=zeros((startfeed+4-endHeader+2),2);
  
  j=1;
  for i=startfeed+4:endHeader-1
      compoundtab(j,1)=str2num(lines{1}{i}(7:9));
      compoundtab(j,2)=str2num(strrep(lines{1}{i}(12:20), ',','.'));
      j=j+1;
  end
  
  %find out if material is solid or gassous
  if (~isempty(find(~cellfun(@isempty,strfind(lines{:}, 'Target is a GAS')),1,'first')))
      a=2; %gassous target
  else
      a=1; %solid target
  end
  
  %calculate effective charge of material
  Zeff=sum(fliplr(compoundtab).*compoundtab/100,1);
  output.Zeff=Zeff(1);
  
  %calculate effective electron density of material
  output.Nele=output.Zeff*atomdensity;
  
  %calculate mean effective ionisation potential
  output.Ieff=compoundtab(:,2)'*meanIoni(compoundtab(:,1),a)/100;
     

  %interpolate linear for energies < table(1,1) in case of Eloss-table
  m=(output.table(2,2)-output.table(1,2))/(output.table(2,1)-output.table(1,1));
  b=output.table(2,2)-m*output.table(2,1);
  if (b<0)
      b=0;
  end
  output.table=flipud(output.table);
  len=length(output.table);
  output.table(len+1,1)=0;
  output.table(len+1,2)=b;
  output.table=flipud(output.table);

  %interpolate linear for energies < rangetable(1,1) in case of range-table
  m=(output.rangetable(2,2)-output.rangetable(1,2))/(output.rangetable(2,1)-output.rangetable(1,1));
  b=output.rangetable(2,2)-m*output.rangetable(2,1);
  if (b<0)
      b=0;
  end
  output.rangetable=flipud(output.rangetable);
  len=length(output.rangetable);
  output.rangetable(len+1,1)=0;
  output.rangetable(len+1,2)=b;
  output.rangetable=flipud(output.rangetable);

  %create natural spline fit on SRIM-eloss-table; write out info parameters
  %of fit; transpose coefficient matrix c => needs to be later
  fit=spline(output.table(:,1),output.table(:,2));
  output.x=fit.breaks;
  output.c=fit.coefs;
  output.l=fit.pieces;
  output.k=fit.order;

  output.c=(output.c)';
  output.x=(output.x)';

  %create natural spline fit on SRIM-range-table; write out info parameters
  %of fit; transpose coefficient matrix c => needs to be later
  fit=spline(output.rangetable(:,1),output.rangetable(:,2));
  output.xrange=fit.breaks;
  output.crange=fit.coefs;
  output.lrange=fit.pieces;
  output.krange=fit.order;

  output.crange=(output.crange)';
  output.xrange=(output.xrange)';

  %create natural spline fit on inverse of SRIM-range-table; write out info parameters
  %of fit; transpose coefficient matrix c => needs to be later
  fit=spline(output.rangetable(:,2),output.rangetable(:,1));
  output.xrangeinv=fit.breaks;
  output.crangeinv=fit.coefs;
  output.lrangeinv=fit.pieces;
  output.krangeinv=fit.order;

  output.crangeinv=(output.crangeinv)';
  output.xrangeinv=(output.xrangeinv)';

  if corr==1 
    %catch original loss-table for active; make a new one for correction/RCF efficiency with same "geometry"
    activeEnorg=output.table(:,1);
    activeEn=activeEnorg(activeEnorg<=maxencorr);
    
    %fit was done in MeV
    corrtablebest=estbest(5)+1-1./((estbest(1)+estbest(2).*(activeEn/1000-estbest(6))+estbest(3).*(activeEn/1000-estbest(6)).^2)).^2.*(log(estbest(1)+estbest(2).*(activeEn/1000-estbest(6))+estbest(3).*(activeEn/1000-estbest(6)).^2+estbest(4).*(activeEn/1000-estbest(6)).^3));
        
    %corrtablebest is automatically one @ maxencorr
    
    %if original loss-table > maxencorr => insert 1 for no correction > maxencorr
    if activeEnorg(end) > maxencorr
        activeEn = activeEnorg;
        corrtablebest = [corrtablebest; ones(length(activeEnorg(activeEnorg>maxencorr)),1)];
    end
    
    %write output back
    output.corrtable=[activeEn, corrtablebest];   
 
    %make a spline fit on correction
    fitcorr=spline(output.corrtable(:,1),output.corrtable(:,2)); 
       
    %best fit
    output.xcorr=fitcorr.breaks;
    output.ccorr=fitcorr.coefs;
    output.lcorr=fitcorr.pieces;
    output.kcorr=fitcorr.order;
    output.ccorr=(output.ccorr)';
    output.xcorr=(output.xcorr)';
   
  end
  
end

%check that second entry in matrix is approx 1keV (was first entry in original srim-file)!
if (output.table(2,1)==0)
    %give error about wrong srim data-table format => must be keV/micron
    set(handles.out_text,'String',['ERROR: SRIM table ' file ' has wrong format; have to be keV/um'],'ForegroundColor','red');
    set(handles.start_pushbutton,'Enable','Off');
    drawnow;
    
elseif ~(output.table(2,1)==0.999999)
     %give warning to gui => common srim table should start from 1keV
     set(handles.out_text,'String',['WARNING: SRIM table ' file ' should have start energy 1keV'],'ForegroundColor','blue');
     drawnow;
end


clear a Zeff densityline atomdensity compoundtab checker endHeader fid i index j lines startfeed unitstr fit m b len corr estbest estlow estup activeEnorg activeEn corrtablebest corrtablelow corrtableup errortable rangetab


end

