function [E_new, E_stop, E_stop_corr] = elo_MD55V2(E_new, H_in_Poly, H_in_Active, H_in_Adhesive, projmass)
% Routine to calculate the energyloss and energydeposition in MD-V2-55

% Copyright (C) 2021 by it's contributors.
% Some rights reserved. See COPYING, CREDITS.
%
% This file is part of RCF Energyloss GUI.
%
% RCF Energyloss GUI is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% long with this program.  If not, see <https://www.gnu.org/licenses/>.

thickness_poly_md = 96;
thickness_active_md = 17.5;
thickness_adhesive_md = 20;
thickness_polymiddle_md = 25;

tmp = elo(E_new, thickness_poly_md, H_in_Poly.x, H_in_Poly.c, H_in_Poly.crange, H_in_Poly.xrangeinv, H_in_Poly.crangeinv, H_in_Poly.k, H_in_Poly.Nele, H_in_Poly.Zeff, H_in_Poly.Ieff, projmass, 0);
E_new = tmp(:,1);

tmp = elo(E_new, thickness_active_md, H_in_Active.x, H_in_Active.c, H_in_Active.crange, H_in_Active.xrangeinv, H_in_Active.crangeinv, H_in_Active.k, H_in_Active.Nele, H_in_Active.Zeff, H_in_Active.Ieff, projmass, 1, H_in_Active.ccorr);
E_new = tmp(:,1);
Estop1 = mean(tmp(:,2));   
E_stop1_corr = mean(tmp(:,3));

tmp = elo(E_new, thickness_adhesive_md, H_in_Adhesive.x, H_in_Adhesive.c, H_in_Adhesive.crange, H_in_Adhesive.xrangeinv, H_in_Adhesive.crangeinv, H_in_Adhesive.k, H_in_Adhesive.Nele, H_in_Adhesive.Zeff, H_in_Adhesive.Ieff, projmass, 0);
E_new = tmp(:,1);

tmp = elo(E_new, thickness_polymiddle_md, H_in_Poly.x, H_in_Poly.c, H_in_Poly.crange, H_in_Poly.xrangeinv, H_in_Poly.crangeinv, H_in_Poly.k, H_in_Poly.Nele, H_in_Poly.Zeff, H_in_Poly.Ieff, projmass, 0);
E_new = tmp(:,1);

tmp = elo(E_new, thickness_adhesive_md, H_in_Adhesive.x, H_in_Adhesive.c, H_in_Adhesive.crange, H_in_Adhesive.xrangeinv, H_in_Adhesive.crangeinv, H_in_Adhesive.k, H_in_Adhesive.Nele, H_in_Adhesive.Zeff, H_in_Adhesive.Ieff, projmass, 0);
E_new = tmp(:,1);

tmp = elo(E_new, thickness_active_md, H_in_Active.x, H_in_Active.c, H_in_Active.crange, H_in_Active.xrangeinv, H_in_Active.crangeinv, H_in_Active.k, H_in_Active.Nele, H_in_Active.Zeff, H_in_Active.Ieff, projmass, 1, H_in_Active.ccorr);
E_new = tmp(:,1);
Estop2 = mean(tmp(:,2));
E_stop2_corr = mean(tmp(:,3));

tmp = elo(E_new, thickness_poly_md, H_in_Poly.x, H_in_Poly.c, H_in_Poly.crange, H_in_Poly.xrangeinv, H_in_Poly.crangeinv, H_in_Poly.k, H_in_Poly.Nele, H_in_Poly.Zeff, H_in_Poly.Ieff, projmass, 0);
E_new = tmp(:,1);

E_stop = Estop1+Estop2;
E_stop_corr = E_stop1_corr+E_stop2_corr;
end